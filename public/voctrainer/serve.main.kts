#!/bin/sh

/*/ __kotlin_script_installer__ 2>&-
# vim: syntax=kotlin
#    _         _   _ _                       _       _
#   | |       | | | (_)                     (_)     | |
#   | | _____ | |_| |_ _ __    ___  ___ _ __ _ _ __ | |_
#   | |/ / _ \| __| | | '_ \  / __|/ __| '__| | '_ \| __|
#   |   < (_) | |_| | | | | | \__ \ (__| |  | | |_) | |_
#   |_|\_\___/ \__|_|_|_| |_| |___/\___|_|  |_| .__/ \__|
#                         ______              | |
#                        |______|             |_|
v=1.6.21.18
p=org/cikit/kotlin_script/"$v"/kotlin_script-"$v".sh
url="${M2_CENTRAL_REPO:=https://repo1.maven.org/maven2}"/"$p"
kotlin_script_sh="${M2_LOCAL_REPO:-"$HOME"/.m2/repository}"/"$p"
if ! [ -r "$kotlin_script_sh" ]; then
  kotlin_script_sh="$(mktemp)" || exit 1
  fetch_cmd="$(command -v curl) -kfLSso" || \
    fetch_cmd="$(command -v fetch) --no-verify-peer -aAqo" || \
    fetch_cmd="wget --no-check-certificate -qO"
  if ! $fetch_cmd "$kotlin_script_sh" "$url"; then
    echo "failed to fetch kotlin_script.sh from $url" >&2
    rm -f "$kotlin_script_sh"; exit 1
  fi
  dgst_cmd="$(command -v openssl) dgst -sha256 -r" || dgst_cmd=sha256sum
  case "$($dgst_cmd < "$kotlin_script_sh")" in
  "c37b0a96dadf8039f63779c0754217f08dd9b811e38794bb1ede500774dac8ab "*) ;;
  *) echo "error: failed to verify kotlin_script.sh" >&2
     rm -f "$kotlin_script_sh"; exit 1;;
  esac
fi
. "$kotlin_script_sh"; exit 2
*/

///DEP=com.github.ajalt.clikt:clikt-jvm:3.4.1

///DEP=org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.6.21
///DEP=org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.6.21

///DEP=io.vertx:vertx-core:4.2.7

///DEP=io.netty:netty-common:4.1.74.Final
///DEP=io.netty:netty-buffer:4.1.74.Final
///DEP=io.netty:netty-transport:4.1.74.Final

///DEP=io.netty:netty-handler:4.1.74.Final
///DEP=io.netty:netty-handler-proxy:4.1.74.Final

///DEP=io.netty:netty-codec:4.1.74.Final
///DEP=io.netty:netty-codec-socks:4.1.74.Final
///DEP=io.netty:netty-codec-http:4.1.74.Final
///DEP=io.netty:netty-codec-http2:4.1.74.Final
///DEP=io.netty:netty-codec-dns:4.1.74.Final

///DEP=io.netty:netty-resolver:4.1.74.Final
///DEP=io.netty:netty-resolver-dns:4.1.74.Final

///DEP=io.vertx:vertx-web:4.2.7
///DEP=io.vertx:vertx-web-common:4.2.7
///DEP=io.vertx:vertx-auth-common:4.2.7
///DEP=io.vertx:vertx-bridge-common:4.2.7

///DEP=io.vertx:vertx-web-client:4.2.7
///DEP=io.vertx:vertx-web-proxy:4.2.7
///DEP=io.vertx:vertx-http-proxy:4.2.7

///DEP=io.vertx:vertx-lang-kotlin-coroutines:4.2.7
///DEP=org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:1.5.2

///DEP=org.eclipse.jgit:org.eclipse.jgit:6.1.0.202203080745-r
///DEP=com.googlecode.javaewah:JavaEWAH:1.1.13

///DEP=org.slf4j:slf4j-api:1.7.32
///RDEP=org.slf4j:slf4j-simple:1.7.32

@file:DependsOn("io.vertx:vertx-web:4.2.7")
@file:DependsOn("io.vertx:vertx-lang-kotlin-coroutines:4.2.7")
@file:DependsOn("com.github.ajalt.clikt:clikt-jvm:3.4.1")

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.UsageError
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.int
import com.github.ajalt.clikt.parameters.types.path
import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.FileSystemAccess
import io.vertx.ext.web.handler.LoggerHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.nio.file.Paths
import kotlin.coroutines.CoroutineContext
import kotlin.io.path.Path
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.name
import kotlin.io.path.writeText

System.setProperty("org.slf4j.simpleLogger.showLogName", "false")
System.setProperty("org.slf4j.simpleLogger.showThreadName", "false")
System.setProperty("org.slf4j.simpleLogger.levelInBrackets", "true")

private val log = LoggerFactory.getLogger("serve")

Path("csv")
    .listDirectoryEntries("*.csv")
    .sorted()
    .joinToString(",") { "\"${it.name}\"" }
    .let { files ->
        Path("dir.json").writeText("{\"files\":[$files]}")
    }

class ServeCommand : CliktCommand(
    name = "server",
    help = "serve static content vi http"
) {
    private val port by option(
        help = "listen on port"
    ).int().default(9003)

    private val static by argument(
        help = "root directory for static files"
    ).multiple(default = listOf("."))

    override fun run() {
        if (static.isEmpty()) {
            throw UsageError("nothing to do. specify root directories!")
        }
        val vx = Vertx.vertx()
        val router = Router.router(vx)
        router.route().handler(LoggerHandler.create())
        for (arg in static) {
            val urlPath = arg.substringBefore(':', "")
            val cacheDir = arg.substringAfter(':')
            val staticHandler = StaticHandler.create(
                FileSystemAccess.ROOT,
                Paths.get(cacheDir).toAbsolutePath().toString()
            )
            router.get("$urlPath/*").handler(staticHandler)
            router.head("$urlPath/*").handler(staticHandler)
        }
        CoroutineScope(vx.dispatcher() as CoroutineContext).launch {
            val server = vx.createHttpServer()
                .requestHandler(router)
                .listen(port ?: 3000)
                .await()
            log.info("started http server on port ${server.actualPort()}")
        }
    }
}

ServeCommand().main(args)
